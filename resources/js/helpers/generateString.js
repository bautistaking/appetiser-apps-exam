export function generateStrings(numberOfStrings, stringLength) {
	const randomstring = require('randomstring')
	const s = new Set()

	while (s.size < numberOfStrings) {
		s.add(randomstring.generate(stringLength))
	}

	for (const value of s.values()) {
	  return value;
	}
}