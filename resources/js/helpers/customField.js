export default {
	"default": {
		defaultValueText: true,
		defaultValueTextarea: false,
		placeholderHolder: true,
		prepend: true,
		append: true,
		characterLimit: true,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"text": {
		defaultValueText: true,
	    defaultValueTextarea: false,
	    placeholderHolder: true,
	    prepend: true,
	    append: true,
	    characterLimit: true,
	    numrows: false,
	    minimumValue: false,
	    maximumValue: false,
	    returnFormat: false,
	    previewSize: false,
	    minimumImageSize: false,
	    maximumImageSize: false,
	    allowedFileTypes: false,
	    minimumFileSize: false,
	    maximumFileSize: false,
	    choices: false,
	    allowNUll: false,
	    selectMultipleValue: false,
	    returnFormatSelect: false,
	    layout: false,
	    toggle: false,
	    message: false,
	    defaultValueSwitch: false,
	  	mapCenter: false,
	  	mapZoom: false,
	  	mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"textarea": {
		defaultValueText: false,
		defaultValueTextarea: true,
		placeholderHolder: true,
		prepend: false,
		append: false,
		characterLimit: true,
		numrows: true,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"number": {
		defaultValueText: true,
		defaultValueTextarea: false,
		placeholderHolder: true,
		prepend: true,
		append: true,
		characterLimit: false,
		numrows: false,
		minimumValue: true,
		maximumValue: true,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"email": {
		defaultValueText: true,
		defaultValueTextarea: false,
		placeholderHolder: true,
		prepend: true,
		append: true,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"url": {
		defaultValueText: true,
		defaultValueTextarea: false,
		placeholderHolder: true,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"image": {
		defaultValueText: false,
		defaultValueTextarea: false,
		placeholderHolder: false,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: true,
		previewSize: true,
		minimumImageSize: true,
		maximumImageSize: true,
		allowedFileTypes: true,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"file": {
		defaultValueText: false,
		defaultValueTextarea: false,
		placeholderHolder: false,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: true,
		previewSize: true,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: true,
		minimumFileSize: true,
		maximumFileSize: true,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"wysiwyg": {
		defaultValueText: false,
		defaultValueTextarea: true,
		placeholderHolder: false,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},	
	"select": {
		defaultValueText: false,
		defaultValueTextarea: true,
		placeholderHolder: false,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: true,
		allowNUll: true,
		selectMultipleValue: true,
		returnFormatSelect: true,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"checkbox": {
		defaultValueText: false,
		defaultValueTextarea: true,
		placeholderHolder: false,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: true,
		layout: true,
		toggle: true,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"radio": {
		defaultValueText: true,
		defaultValueTextarea: false,
		placeholderHolder: false,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: true,
		selectMultipleValue: false,
		returnFormatSelect: true,
		layout: true,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"true_false": {
		defaultValueText: false,
		defaultValueTextarea: false,
		placeholderHolder: false,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: true,
		defaultValueSwitch: true,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"google_map": {
		defaultValueText: false,
		defaultValueTextarea: false,
		placeholderHolder: false,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: true,
		mapZoom: true,
		mapHeight: true,
		dateDisplayFormat: false,
		dateReturnFormat: false,
		weekStartOn: false
	},
	"date_picker": {
		defaultValueText: false,
		defaultValueTextarea: false,
		placeholderHolder: false,
		prepend: false,
		append: false,
		characterLimit: false,
		numrows: false,
		minimumValue: false,
		maximumValue: false,
		returnFormat: false,
		previewSize: false,
		minimumImageSize: false,
		maximumImageSize: false,
		allowedFileTypes: false,
		minimumFileSize: false,
		maximumFileSize: false,
		choices: false,
		allowNUll: false,
		selectMultipleValue: false,
		returnFormatSelect: false,
		layout: false,
		toggle: false,
		message: false,
		defaultValueSwitch: false,
		mapCenter: false,
		mapZoom: false,
		mapHeight: false,
		dateDisplayFormat: true,
		dateReturnFormat: true,
		weekStartOn: true
	}
}