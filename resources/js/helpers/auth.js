import { setAuthorization } from "./general";

export function login(credentials) {
    return new Promise((res, rej) => {        
        axios.post('/api/v1/login', credentials)
            .then((response) => {
                setAuthorization(response.data.data.token);
                res(response.data.data);
            })                    
    })
}

export function getLocalUser() {
    const userStr = localStorage.getItem("user");
    if(!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}