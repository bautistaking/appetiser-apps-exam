import web from './views/AdminLayout/TheMaster.vue';
import index from './views/Admin/Index.vue';
import login from './views/Admin/Auth/Login.vue';

export const routes = [
    {
        path: '/login',
        component: login,
        name: 'login'
    },
    {
        path: '/',
        component: web,
        name: 'exam',
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: '/',
                component: index
            }
        ]
    },
];