<?php

return [
    'ORDER_STATUS' => [
        'PENDING'                   => 1,
        'SHIPPED'                   => 2,
        'PARTIALLY_SHIPPED'         => 3,
        'DELIVERED'                 => 4,
        'PARTIALLY_DELIVERED'       => 4,
        'CANCELLED'                 => 6,
        'IN_PROCESS'                => 7,
        'ON_HOLD'                   => 8,
        'RETURNED'                  => 9,
        'COD'                       => 10,
        'PICKUP_COMPLETED'          => 31,
        'READY_TO_SHIP'             => 32,
        'IN_TRANSIT'                => 33,
        'FAILED_TO_DELIVER'         => 34,
        'REJECTED'                  => 35,
        'FOR_REPLACEMENT'           => 36,
        'ORDER_FULFILLED_BY_ADM'    => 37
    ],

    'PAYNAMICS' => [
        'FORM_ACTION_URL'       => 'https://testpti.payserv.net/webpayment/default.aspx',
        'MERCHANT_ID'           => '000000260716E6B3B8BE',
        'MERCHANT_KEY'          => '6CD102D300E12216660691CC5A2725B6',
        'IP_ADDRESS'            => '52.77.189.92',
        'CURRENCY'              => 'PHP',
        'COUNTRY'               => 'PH',
        'SEC3D'                 => 'try3d' //enabled or try3d
    ],

    'PAYNAMICS_PROD' => [
        'FORM_ACTION_URL'       => 'https://ptiapps.paynamics.net/webpayment/innerworks/Default.aspx',
        'MERCHANT_ID'           => '0000001010160BF453E9',
        'MERCHANT_KEY'          => '8039BDECBFB4AE676FF898CCCE421958',
        'IP_ADDRESS'            => '52.77.189.92',
        'CURRENCY'              => 'PHP',
        'COUNTRY'               => 'PH',
        'SEC3D'                 => 'try3d' //enabled or try3d
    ],

    'LF' => [
        'JWT' => env('LF_JWT')
    ],

    'ADMIN' => [
        'EMAIL' => [
            'ccchan@adobomall.com',
            'dmcarreon@adobomall.com',
            'evyu@adobomall.com',
            'inquiry@adobomall.com',
            'wsyoung@adobomall.com',
            'gcng@adobomall.com',
        ],
        'TEST' => [
            'adobomall.dev@gmail.com'
        ]
    ]
];

