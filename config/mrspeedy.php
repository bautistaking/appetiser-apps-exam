<?php

return [

    'environment' => env('MR_SPEEDY_ENVIRONMENT'),

    'api_test_url' => env('MR_SPEEDY_API_TEST_URL'),

    'test_callback_url' => env('MR_SPEEDY_TEST_CALLBACK_URL'),

    'test_sercret_auth_token' => env('MR_SPEEDY_TEST_SECRET_AUTH_TOKEN'),

    'test_callback_auth_token' => env('MR_SPEEDY_TEST_CALLBACK_AUTH_TOKEN'),

    'api_prod_url' => env('MR_SPEEDY_API_PROD_URL'),

    'prod_callback_url' => env('MR_SPEEDY_PROD_CALLBACK_URL'),

    'prod_secret_auth_token' => env('MR_SPEEDY_PROD_SECRET_AUTH_TOKEN'),

    'prod_callback_auth_token' => env('MR_SPEEDY_PROD_CALLBACK_AUTH_TOKEN'),

];
