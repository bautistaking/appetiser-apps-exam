<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () 
{
    /*
    |--------------------------------------------------------------------------
    | Auth Routes
    |--------------------------------------------------------------------------
    */    
    Route::post('login', 'App\AuthController@login')->name('api.v1.employee.login');
});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api']], function () 
{
    /*
    |--------------------------------------------------------------------------
    | Auth Routes
    |--------------------------------------------------------------------------
    */
    Route::get('logout', 'App\AuthController@logout')->name('api.v1.employee.logout');   

    /*
    |--------------------------------------------------------------------------
    | Employee Routes
    |--------------------------------------------------------------------------
    */
    Route::post('event/store', 'App\EventController@store')->name('api.v1.event.store');
    Route::get('event/list', 'App\EventController@list')->name('api.v1.event.list');
});