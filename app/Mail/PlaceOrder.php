<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PlaceOrder extends Mailable
{
    use Queueable, SerializesModels;

    protected $reference_number;
    protected $name;
    protected $address;
    protected $phone;
    protected $email;
    protected $orders;
    protected $total;
    protected $delivery_charge;
    protected $status;
    protected $date_placed;
    protected $payment_type;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->reference_number = $details['order_number'];
        $this->name = $details['name'];
        $this->address = $details['address'];
        $this->phone = $details['phone'];
        $this->email = $details['email'];
        $this->orders = $details['orders'];
        $this->total = $details['total'];
        $this->status = $details['status'];
        $this->delivery_charge = $details['delivery_charge'];
        $this->subject = $details['subject'];
        $this->date_placed = $details['date_placed'];
        $this->payment_type = $details['payment_type'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.place.order')
            ->with([
                'date_placed' => $this->date_placed,
                'reference_number' => $this->reference_number,
                'name' => $this->name,
                'address' => $this->address,
                'phone' => $this->phone,
                'email' => $this->email,
                'orders' => $this->orders,
                'delivery_charge' => $this->delivery_charge,
                'status' => $this->status,
                'total' => $this->total,
                'payment_type' => $this->payment_type,
                //'url' => route('contact')
            ])
            ->subject($this->subject);
    }
}
