<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are soft delete.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'salt', 
        'active', 
        'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token', 
        'activation_token', 
        'salt'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'users';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Set Date format.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d';

    public static function getSalt($email)
    {
        $user = static::where('email', '=', $email)->first();
        return $user->salt;
    }

    public function assignRole($roleId)
    {
        $roles = UserRole::find($this->id);
        if($roles)
            $roles->delete();

        $userRoles = array('user_id' => $this->id, 'role_id' => $roleId);
        if(UserRole::updateOrCreate($userRoles))
            return true;
        return false;
    }

    public function addMeta($metaData)
    {
        foreach ($metaData as $key => $detail) {
            $where = [
                ['user_id', $this->id],
                ['meta_key', $key]        
            ];            
            $user_meta = UserMeta::where($where)->first();

            if(!$user_meta)
                $user_meta = new UserMeta;
                $user_meta->user_id = $this->id;
                $user_meta->meta_key = $key;
                $user_meta->meta_value = $detail;
                $user_meta->save();

            $user_meta->meta_key = $key;
            $user_meta->meta_value = $detail;
            $user_meta->save();
        }
    }
}
