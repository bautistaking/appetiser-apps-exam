<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Requests\Auth\LoginRequest;

interface AuthControllerInterface
{
	/**
	 * @param string $username
	 * @param string $password
	 * @return Response
	 * @SWG\Post(
	 *      path="/login",
	 *      summary="Login",
	 *      tags={"Mobile App - Auth"},
	 *      description="Login",
	 *      produces={"application/json"},
	 *       @SWG\Parameter(
	 *          name="email",
	 *          description="Email",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="password",
	 *          description="Password",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */    
    public function login(LoginRequest $request);  

 	/**
	 * @return Response
	 * @SWG\Get(
	 *      path="/logout",
	 *      summary="Sign out",
	 *      tags={"Mobile App - Auth"},
	 *      description="Sign out",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */ 
    public function logout(Request $request);   
}
