<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as ApiBaseController;
use App\Models\Event;

use Carbon\CarbonPeriod;
use Carbon\Carbon;

class EventController extends ApiBaseController implements EventControllerInterface
{
    public function store(Request $request)
    {
    	try
    	{
            $event = new Event;
            $event->event = $request->event;
            $event->date_from = $request->date_from;
            $event->date_to = $request->date_to;
            $event->monday = $this->isBoolean($request->monday);
            $event->tuesday = $this->isBoolean($request->tuesday);
            $event->wednesday = $this->isBoolean($request->wednesday);
            $event->thursday = $this->isBoolean($request->thursday);
            $event->friday = $this->isBoolean($request->friday);
            $event->saturday = $this->isBoolean($request->saturday);
            $event->sunday = $this->isBoolean($request->sunday);
            $event->save();

            $period = CarbonPeriod::create($request->date_from, $request->date_to);
            foreach($period as $date)
            {

                if($date->format('D') == 'Mon' && $this->isBoolean($request->monday) == true)
                    $dates[] = ['date' => $date->format('d D'), 'event' => $request->event];
                else if($date->format('D') == 'Tue' && $this->isBoolean($request->tuesday) == true)
                    $dates[] = ['date' => $date->format('d D'), 'event' => $request->event];
                else if($date->format('D') == 'Wed' && $this->isBoolean($request->wednesday) == true)
                    $dates[] = ['date' => $date->format('d D'), 'event' => $request->event];
                else if($date->format('D') == 'Thu' && $this->isBoolean($request->thursday) == true)
                    $dates[] = ['date' => $date->format('d D'), 'event' => $request->event];
                else if($date->format('D') == 'Fri' && $this->isBoolean($request->friday) == true)
                    $dates[] = ['date' => $date->format('d D'), 'event' => $request->event];
                else if($date->format('D') == 'Sat' && $this->isBoolean($request->saturday) == true)
                    $dates[] = ['date' => $date->format('d D'), 'event' => $request->event];
                else if($date->format('D') == 'Sun' && $this->isBoolean($request->wednesday) == true)
                    $dates[] = ['date' => $date->format('d D'), 'event' => $request->sunday];
                else 
                    $dates[] = ['date' => $date->format('d D'), 'event' => ''];
            }

            $now = Carbon::now();
            $data = [
                'monthYear' => $now->format('F'). ' '. $now->year,
                'dates' => $dates
            ];

            return $this->response($data, 'Successfully Created!', $this->successStatus);
    	}
    	catch (\Exception $e) 
        {
    		 return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
    	}
    }

    public function isBoolean($value)
    {
    	if(!$value)
    		return false;
    	if(is_string($value))
    		return ($value == 'true') ? 1 : 0;
    	return $value;
    }

    public function list()
    {
        try
        {
            $start = new Carbon('first day of this month');
            $end = new Carbon('last day of this month');

            $period = CarbonPeriod::create($start, $end);
            foreach($period as $date)
            {
                $dates[] = ['date' => $date->format('d D'), 'event' => ''];
            }

            $now = Carbon::now();
            $data = [
                'monthYear' => $now->format('F'). ' '. $now->year,
                'dates' => $dates
            ];

            return $this->response($data, 'Successfully Created!', $this->successStatus);
        }
        catch (\Exception $e) 
        {
             return response([
                'message' => $e->getMessage(),
                'status' => false,
                'status_code' => $this->unauthorizedStatus,
            ], $this->unauthorizedStatus);
        }
    }

}
