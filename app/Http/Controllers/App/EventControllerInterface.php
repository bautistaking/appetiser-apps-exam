<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

interface EventControllerInterface
{
    /**
	 * @param string $event
	 * @param string $date_from
	 * @param boolean $monday
	 * @param boolean $tuesday
	 * @param boolean $wednesday
	 * @param boolean $thursday
	 * @param boolean $friday
	 * @param boolean $saturday
	 * @param boolean $sunday
	 * @return Response
	 * @SWG\Post(
	 *      path="/event/store",
	 *      summary="Store / Save new event",
	 *      tags={"Events"},
	 *      description="Store / Save new event",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *       @SWG\Parameter(
	 *          name="event",
	 *          description="Event",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="date_from",
	 *          description="Date From",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="date_to",
	 *          description="Date To",
	 *          type="string",
	 *          required=true,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="monday",
	 *          description="monday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="tuesday",
	 *          description="tuesday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="wednesday",
	 *          description="wednesday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="thursday",
	 *          description="thursday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="friday",
	 *          description="friday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="saturday",
	 *          description="saturday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *       @SWG\Parameter(
	 *          name="sunday",
	 *          description="sunday",
	 *          type="boolean",
	 *          required=false,
	 *          in="formData"
	 *      ),
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */  
    public function store(Request $request);

    /**
	 * @return Response
	 * @SWG\Get(
	 *      path="/event/list",
	 *      summary="List of dates",
	 *      tags={"Events"},
	 *      description="List of dates",
	 *      produces={"application/json"},
	 *      security={
	 *         {
	 *             "default": {}
	 *         }
	 *      },
	 *      @SWG\Response(
	 *          response=200,
	 *          description="successful operation",
	 *          @SWG\Schema(
	 *              type="object",
	 *              @SWG\Property(
	 *                  property="data",
	 *                  type="object"
	 *              ),
	 *              @SWG\Property(
	 *                  property="message",
	 *                  type="string"
	 *              ),
	 *              @SWG\Property(
	 *                  property="status_code",
	 *                  type="integer"
	 *              ),
	 *              @SWG\Property(
	 *                  property="success",
	 *                  type="boolean"
	 *              ),
	 *              @SWG\Property(
	 *                  property="resource",
	 *                  type="string"
	 *              )
	 *          )
	 *      )
	 * )
	 */  
    public function list();
}
