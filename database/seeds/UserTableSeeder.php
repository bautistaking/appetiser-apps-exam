<?php

use Illuminate\Database\Seeder;
use App\Services\Helpers\PasswordHelper;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salt = bcrypt(PasswordHelper::generateSalt());

        $input['name'] = 'Administrator';
        $input['email'] = 'admin@gmail.com';
        $input['password'] = 'password';
        $input['salt'] = $salt;
        $input['password'] = PasswordHelper::generate($salt, $input['password']);
        $input['active'] = 1;

        User::create($input);        
    }
}
